FROM nvidia/cuda:11.4.3-base-ubuntu20.04

RUN apt update
RUN apt-get install -y python3 python3-pip

RUN pip install pika 
RUN pip install transformers
RUN pip install torch
RUN pip install tensorflow